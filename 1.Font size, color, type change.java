package com.example.sivanesh.fontandcolor;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView text;
    Button increaseFontSize, decreaseFontSize, fontChange;
    int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text = (TextView)findViewById(R.id.textView);
        increaseFontSize = (Button)findViewById(R.id.increaseFontSize);
        decreaseFontSize = (Button)findViewById(R.id.decreaseFontSize);
        fontChange = (Button)findViewById(R.id.fontChange);


        increaseFontSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                text.setTextSize(TypedValue.COMPLEX_UNIT_PX, text.getTextSize() + 5);
            }
        });

        decreaseFontSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                text.setTextSize(TypedValue.COMPLEX_UNIT_PX, text.getTextSize() - 5);
                text.setTextColor(Color.BLUE);
            }
        });


        fontChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count++;
                switch(count % 3) {
                    case 0:
                        text.setTypeface(Typeface.SANS_SERIF);count++;
                        break;
                    case 1:
                        text.setTypeface(Typeface.MONOSPACE);count++;
                        break;
                    case 2:
                        text.setTypeface(Typeface.DEFAULT_BOLD);count++;
                        break;
                    default:
                        count = 0;
                        text.setTypeface(Typeface.SERIF); count++;
                }
            }
        });
    }
}
