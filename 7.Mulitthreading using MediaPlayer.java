package com.example.sivanesh.audioplayer3;

import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Time;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    SeekBar seekbar;
    Button play, pause, forward, backward, prev, next;
    TextView songName, time;
    MediaPlayer player;


    // Player variables
    double startTime, finalTime;
    boolean bool = false;
    Handler h = new Handler();
    int forwardTime = 5000, backwardTime = 5000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        seekbar = (SeekBar)findViewById(R.id.seekBar);
        play = (Button)findViewById(R.id.playButton);
        pause = (Button)findViewById(R.id.pauseButton);
        songName = (TextView)findViewById(R.id.songName);
        time = (TextView)findViewById(R.id.time);
        forward = (Button)findViewById(R.id.forward);
        backward = (Button)findViewById(R.id.backward);
        prev = (Button)findViewById(R.id.prev);
        next = (Button)findViewById(R.id.next);

        player = MediaPlayer.create(MainActivity.this, R.raw.never_give_up);
        songName.setText("Never Give up.mp3");

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!player.isPlaying()) {
                    startTime = player.getCurrentPosition();
                    finalTime = player.getDuration();
                    player.start();
                    // INorder to assign this only once for a song
                    if(!bool) {
                        seekbar.setMax((int) finalTime);
                        bool = true;
                    }
                    h.postDelayed(UpdateSongTime, 100);
                }
            }
        });

        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(player.isPlaying()) {
                    player.pause();
                }
            }
        });

        forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int temp = (int)startTime;
                if(temp + forwardTime < player.getDuration()) {
                    startTime += forwardTime;
                    player.seekTo((int) startTime);
                    Toast.makeText(getApplicationContext(), "Forwarded 5 seconds", Toast.LENGTH_SHORT);
                }
            }
        });

        backward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int temp = (int) startTime;
                if(temp - backwardTime > 0) {
                    startTime -= backwardTime;
                    player.seekTo((int) startTime);
                    Toast.makeText(getApplicationContext(), "Backwarded 5s", Toast.LENGTH_SHORT);
                }
            }
        });

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                player.seekTo(0);

            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                player.seekTo(player.getDuration());

            }
        });
    }



    private Runnable UpdateSongTime = new Runnable() {
      public void run() {
          time.setText(TimeUnit.MILLISECONDS.toMinutes((long) startTime) + ":" + (
                  TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
                  TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) startTime))

          ));
          startTime = player.getCurrentPosition();
          seekbar.setProgress((int)startTime);
          h.postDelayed(this, 100);
      }
    };
}
