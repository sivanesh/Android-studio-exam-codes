/*
Add them in Manifest file
<uses-permission android:name="android.permission.SEND_SMS" />

*/

package com.example.sivanesh.message;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = (Button)findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    SmsManager smsManager = SmsManager.getDefault();
                    smsManager.sendTextMessage("<Phone Number>", null, "Hello completed", null, null);
                    Toast.makeText(getApplicationContext(), "Sended successfully", Toast.LENGTH_SHORT).show();

                } catch(Exception er) {
                    Toast.makeText(getApplicationContext(), "Cant send message", Toast.LENGTH_SHORT).show();
                    System.out.println(er);
                }
            }
        });




    }
}
