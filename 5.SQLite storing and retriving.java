package com.example.sivanesh.databaseconnectivity;
/*
Comment either read or wirte or change both their variable names inorder to compile successfully

*/
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Writable database
        DBHelper helper = new DBHelper(this);
        SQLiteDatabase db = helper.getWritableDatabase();

            // ContentValues implements HashMap hence using put()
        ContentValues values = new ContentValues();
        values.put("name", "Sivanesh");
        values.put("age", "20");

        long row = db.insert("student", null, values);
        System.out.println("\n\nROw number is = " + row);


        // Readable Database
        DBHelper helper = new DBHelper(this);
        SQLiteDatabase db = helper.getReadableDatabase();
        String[] columnNames = {"name", "age"};


            // db.query() gets collection of datas
            // Cursor is used to do manipulations on those queries
        Cursor c = db.query("student", columnNames, null, null, null, null, null);
        c.moveToFirst();
        System.out.println("Name is = " + c.getString(0));


    }
}

class DBHelper extends SQLiteOpenHelper {

    public DBHelper(Context context) {
//        super(context, name, factory, version);
        super(context, "student.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table student (name text, age text)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("drop table if exists student");
        onCreate(db);
    }
}