package com.example.sivanesh.singledatabase;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Creating database and table
        db = openOrCreateDatabase("dbName", MODE_PRIVATE, null);
        db.execSQL("create table if not exists student (name text, age number)");

        // Inserting a record
        ContentValues cv = new ContentValues();
        cv.put("name", "DEmo");
        cv.put("age", 11);
        db.insert("student", null, cv);
        System.out.println("Successfully inserted");

        ContentValues cv2 = new ContentValues();
        cv2.put("name", "Rider");
        cv2.put("age", 15);
        db.insert("student", null, cv2);
        System.out.println("Successfully inserted");

        // viewing a record
            // Cursor used to iterate or control queries
        String columns[] = {"name", "age"};
        Cursor c  = db.query("student", columns, null, null, null, null, null);
        if(c.moveToFirst()) {
            do {
                System.out.println("Name = " + c.getString(0) + "==== Age = " + c.getString(1));

            }while(c.moveToNext());
        }

    }
}
