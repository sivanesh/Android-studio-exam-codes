package com.example.sivanesh.alarm;
/*
    Caution: this is not the real way to create an alarm but it can do stuff what we wanna achieve

*/
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText time;
    Button button;

    MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = (Button)findViewById(R.id.button);
        time = (EditText)findViewById(R.id.time);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert();
            }
        });
    }

    protected void alert() {
        int seconds = Integer.parseInt(time.getText().toString());

        Handler h = new Handler();

        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                MediaPlayer.create(getApplicationContext(), R.raw.tik_tik_tik).start();
            }
        }, (seconds * 1000));
    }
}
