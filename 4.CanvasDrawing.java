package com.example.sivanesh.canvas;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    DemoView dv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dv = new DemoView(this);
        setContentView(dv);
    }
}

class DemoView extends View {
    DemoView (Context context) {
        super(context);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint p = new Paint();

        // Creating a canvas of page size
        p.setStyle(Paint.Style.FILL);
        p.setColor(Color.BLACK);
        canvas.drawPaint(p);

        // Drawing a line
        p.setColor(Color.WHITE);
        canvas.drawLine(10, 10, 100, 100, p);

        // Draw circle
        p.setColor(Color.YELLOW);
        canvas.drawCircle(150, 150, 30, p);

        // Draw Rectangle
        p.setColor(Color.MAGENTA);
        canvas.drawRect(10, 200, 100, 300, p);
    }
}